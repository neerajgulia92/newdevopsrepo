provider "aws" {
  region="us-east-1"
}

resource "aws_instance" "web" {
  subnet_id = "subnet-034c6a77361bb4906"
  count = "1"
  instance_type = "t2.micro"
  ami = "ami-035be7bafff33b6b6"
  key_name = "EFS_key"
  vpc_security_group_ids= ["sg-06f2c0bd3e2dcd084"]


  # We set the name as a tag
  tags {
    "Name" = "TestCaseTF"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.web.private_ip} >> private_ips.txt"
  }
}
